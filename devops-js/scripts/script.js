const fs = require('fs-extra')
const inquirer = require('inquirer')
const exec = require('./utils/exe')
const validateInputenv = require('./utils/validate')
;(async function () {
    const envName = await inquirer.prompt([
        {
            type: 'input',
            name: 'envName',
            message: 'Enter the env filename on the root folder 👉 ',
            default: '.env',
            validate: validateInputenv,
        },
    ])
    
    fs.writeFileSync('data.js', `module.exports.envName = '${Object.values(envName)}'`)
    await exec('node utils/index.js', 'Converting the env variables to yaml files...')
})()
