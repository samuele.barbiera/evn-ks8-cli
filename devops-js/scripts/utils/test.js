'use strict'
const fs = require('fs-extra')
let envName = require('../data.js')
const res = require('dotenv').config({ path: `../../${envName}` })

const data = res.parsed
for (const [key, value] of Object.entries(data)) {
    fs.appendFile('envValues.ejs', `${key}: '${value}'\n`)
}

const envAllData = fs.readFileSync('envValues.ejs', 'utf8')
console.log('🚀 - file: index.js - line 59 - envAllData', envAllData)
