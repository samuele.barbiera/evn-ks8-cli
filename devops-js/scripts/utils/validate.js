/* eslint-disable no-unused-vars */
'use strict'

/**
 * Validates user input for input prompts
 * @param {String} userInput
 * @returns {Boolean}
 */

module.exports = (userInput) => {
    if (!userInput) {
        return `Can't be empty!`
    }
    return true
}
