require 'thor'
require 'erb'
require 'yaml'
require 'dotenv'
require 'fileutils'
require 'base64'

class Erberer < Thor
	option :to_dir, default: '.'
	option :env_file, default: '.env'

	desc "compile SOURCE_DIR", "duplicate a directory compiling the erb files"
	long_desc <<-LONGDESC
	  Duplicate a directory compiling the erb files, if present.
		\x5The vars in erb files must be in the form <%= vars['NAME']%>.
		\x5Then in the .env file NAME=Bruce.\n
		Options:
		\x5--to_dir destination directory. You will find the results in `erbered` subdir.
		\x5--env_file env file with var values\n
	LONGDESC

	def compile(source_dirpath)
		unless File.exists? source_dirpath
			puts "dir #{source_dirpath} not found. Exit."
			exit
		end

		if options[:env_file]
			if File.exists? options[:env_file]
				vars = Dotenv::Environment.new(options[:env_file], true)
			else
				puts "[WARNIG] env file #{options[:env_file]} not found. Ignored."
			end
		end

		#destination_dirpath = File.join options[:to_dir], 'erbered'
		destination_dirpath = source_dirpath

		#source_file_selector = File.join(source_dirpath, '**', '*yml')
		source_file_selector = "#{source_dirpath}/**/*yaml*"

		puts ""
		puts "-------------------------------------------"
		puts "#{source_dirpath} => ~~#{options[:env_file]}~~ => #{destination_dirpath}"
		puts "-------------------------------------------"

		Dir.glob(source_file_selector).each do |source_filepath|
			filecontent = File.read source_filepath
			result_filepath = File.join(destination_dirpath, source_filepath)
			result_dirpath = File.join(result_filepath.split('/')[0...-1])
			FileUtils.mkdir_p result_dirpath

			puts "#{source_filepath} => #{result_filepath.sub('.erb', '')}"

			if source_filepath.end_with?('.erb')
				result_filepath.chomp!('.erb')
				tpl = ERB.new(filecontent)
				result = tpl.result binding
			else
				result = filecontent
			end

			File.write(result_filepath, result)
		end
		puts "-------------------------------------------"
	end

	def self.exit_on_failure?
		true
	end
end

Erberer.start(ARGV)
