## Quick starting point 🔮

### 👇 Base installation on the script dir 👇

```bash
npm i
```

### 👇 Starting the conversion from the env file to yaml files 👇

```bash
cd devops-js/script && npm run start
```

### Specifications ⚡
-   **Env file placement** put the env file inside **./devops-js**
-   **Env Encription** to make sure that certain veriables are encripted to **base64 encoding** just put this string on the key of the variable `SECRET_ENCODE`

### Installation & Prerequisite (to run the cli) ⚗️

-   [**Nodejs**](https://nodejs.org/en/) The JavaScript run-time environment that executes the javaScript code in the cli
-   [**Npm**](https://www.npmjs.com/) the main package manager used for this project.
