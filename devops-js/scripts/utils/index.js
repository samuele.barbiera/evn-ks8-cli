#! /usr/bin/env node
/* eslint-disable no-useless-catch */
/* eslint-disable no-empty-pattern */
/* eslint-disable no-unused-vars */
/* eslint-disable quotes */
'use strict'
const Handlebars = require('handlebars')
const path = require('path')
const fs = require('fs')
let envName = require('../data.js')
const res = require('dotenv').config({path: `../${Object.values(envName)}`})
const data = res.parsed

if (res.error) throw res.error

const searchRecursive = (dir, pattern) => {
    let pathData = []

    fs.readdirSync(dir).forEach(function (dirInner) {
        dirInner = path.resolve(dir, dirInner) // Obtain absolute path
        const stat = fs.statSync(dirInner) // Get stats to determine if path is a directory or a file

        // If path is a directory, scan it and combine results else push files
        if (stat.isDirectory()) {
            pathData = pathData.concat(searchRecursive(dirInner, pattern))
        } else if (stat.isFile() && dirInner.endsWith(pattern)) {
            pathData.push(dirInner)
        }
    })

    return pathData
}

;(async function () {
    let resultsEjs = []
    fs.readdirSync('../../').forEach(function (dirInner) {
        dirInner = path.resolve('../../', dirInner) // Obtain absolute path
        const stat = fs.statSync(dirInner) // Get stats to determine if path is a directory or a file

        // If path is a directory, scan it and combine results else push files
        if (stat.isDirectory()) {
            resultsEjs = resultsEjs.concat(searchRecursive(dirInner, '.ejs'))
        } else if (stat.isFile() && dirInner.endsWith('.ejs')) {
            resultsEjs.push(dirInner)
        }
    })

    for (const [key, value] of Object.entries(data)) {
        if (key.includes('SECRET_ENCODE')) {
            data[key] = Buffer.from(value).toString('base64')
        }
    }

    // for every path of any ejs file create a copy with the .yaml extension
    for (let i in resultsEjs) {
        let result_filepath = path.join(resultsEjs[i])
        const source = fs.readFileSync(result_filepath).toString('utf8')
        const template = Handlebars.compile(source, {noEscape: true})
        const contents = template(data)
        console.log('🚀 - file: index.js - line 59 - contents', contents)
        fs.writeFileSync(`${resultsEjs[i]}\.yaml`, contents)
    }
})()
